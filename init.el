(defconst *is-a-mac* (eq system-type 'darwin))
;; Suppress GUI features
(setq use-file-dialog nil)
(setq use-dialog-box nil)
(setq inhibit-startup-screen t)
;; inhibit welcome page
(setq inhibit-startup-message t)
(setq-default
 initial-scratch-message
 (concat ";; Happy hacking, "user-login-name " - Emacs ♥ you!\n\n"))

;; show column number
(setq column-number-mode t)
;;; set default font
(if *is-a-mac*
    (add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono-16"))
  (add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono-12")))

(setq browse-url-new-window-flag t)
(if *is-a-mac*
    (setq browse-url-browser-function 'browse-url-chrome)
  (setq browse-url-browser-function 'browse-url-firefox
        browse-url-firefox-new-window-is-tab t))

;; load config under `lisp/`
(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))

;; Window size and features
(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))
(when (fboundp 'set-scroll-bar-mode)
  (set-scroll-bar-mode nil))

(if *is-a-mac*
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (unless (display-graphic-p frame)
                  (set-frame-parameter frame 'menu-bar-lines 0))))
  (when (fboundp 'menu-bar-mode)
    (menu-bar-mode -1)))

(let ((no-border '(internal-border-width . 0)))
  (add-to-list 'default-frame-alist no-border)
  (add-to-list 'initial-frame-alist no-border))

;; get rid of gaps
;; Non-nil means resize frames pixelwise.
;; If this option is nil, resizing a frame rounds its sizes to the frame's
;; current values of `frame-char-height' and `frame-char-width'.  If this
;; is non-nil, no rounding occurs, hence frame sizes can increase/decrease
;; by one pixel.
;; With some window managers you have to set this to non-nil in order to
;; fully maximize frames.  To resize your initial frame pixelwise,
;; set this option to a non-nil value in your init file.
(setq frame-resize-pixelwise t)

(setq frame-title-format
      '((:eval (if (buffer-file-name)
                   (abbreviate-file-name (buffer-file-name))
                 "%b"))))

;; Allow access from emacsclient
(add-hook 'after-init-hook
          (lambda ()
            (require 'server)
            (unless (server-running-p)
              (server-start))))

;; load other initialization configs
(require 'init-elpa)

;; sorted by name
(require 'init-cc)
(require 'init-chinese)
(require 'init-company)
(require 'init-docker)
(require 'init-doxygen)
(require 'init-edit)
(require 'init-ess)
(require 'init-flycheck)
(require 'init-git)
(require 'init-ibuffer)
(require 'init-isearch)
(require 'init-ivy)
(require 'init-julia)
;; (require 'init-langtool)
(require 'init-latex)
;; (require 'init-matlab)
(require 'init-mini)
(require 'init-mu4e)
(require 'init-org)
(require 'init-osx)
(require 'init-pdf)
(require 'init-pdf)
(require 'init-polymode)
(require 'init-projectile)
(require 'init-python)
(require 'init-snippet)
(require 'init-spell)
(require 'init-tags)
(require 'init-term)
(require 'init-theme)
(require 'init-whitespace)
(require 'init-window)
(require 'init-yaml)

;;; set global key for most programming languages
(global-set-key (kbd "M--") " = ")

(provide 'init)
;;; init ends here
