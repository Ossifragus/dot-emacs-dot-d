;;; use graphviz-dot-mode
(use-package graphviz-dot-mode
  :ensure t
  :config
  (setq graphviz-dot-indent-width 4)
  (setq graphviz-dot-auto-indent-on-semi nil)
  (setq electric-graphviz-dot-open-brace nil)
  (setq electric-graphviz-dot-close-brace nil))

(provide 'init-graphviz)
;;; init-graphviz.el ends here
