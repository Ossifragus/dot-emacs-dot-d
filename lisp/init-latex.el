;; using auctex
(use-package tex
  :ensure auctex)

;; Turn on RefTeX in AUCTeX
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)

;; Activate nice interface between RefTeX and AUCTeX
(setq reftex-plug-into-AUCTeX t)

;;; add company backend for latex math
(use-package company-math
  :ensure t)
(eval-after-load 'company
  '(add-to-list 'company-backends 'company-math-symbols-unicode))

;;; using ivy-bibtex
(use-package ivy-bibtex
  :ensure t)
(global-set-key (kbd "M-s b") 'ivy-bibtex)

(setq bibtex-completion-bibliography
      '("~/bibrary/bib/index.bib"))
(setq bibtex-completion-library-path
      '("~/bibrary/papers"
        "~/bibrary/books"
        "~/bibrary/misc"))

;;; I do not use notes anyway
;; (setq bibtex-completion-notes-path
;;       "~/bibrary/notes.org")
(setq bibtex-completion-notes-path nil)
(setq bibtex-completion-pdf-field nil)
(setq bibtex-completion-find-additional-pdfs t)
(setq bibtex-completion-pdf-extension '(".pdf" ".djvu"))
(unless *is-a-mac*
  (setq bibtex-completion-pdf-open-function
        (lambda (fpath)
          (call-process "okular" nil 0 nil fpath))))

;;; using gsholar-bibtex
(use-package gscholar-bibtex
  :ensure t)

;;; auto run bib-validate-globally whenever saving bibtex files
(defun bibtex-auto-validate-globally ()
  "Run bibtex-validate-globally after saving the bibtex files."
  (when (eq major-mode 'bibtex-mode)
    (bibtex-validate-globally)))
(add-hook 'after-save-hook #'bibtex-auto-validate-globally)

(provide 'init-latex)
