(setq python-shell-interpreter "python3")

;; elpy for python
(use-package elpy
  :ensure t
  :config (elpy-enable))

(provide 'init-python)
