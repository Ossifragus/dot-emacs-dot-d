(require 'ispell)

;; Add spell-checking in comments for all programming language modes
;; (add-hook 'prog-mode-hook 'flyspell-prog-mode)

(provide 'init-spell)
;;; init-spell.el ends here
