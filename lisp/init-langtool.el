;; if languagetool is available
(if (executable-find "languagetool")
    (use-package langtool
      :ensure t
      :config
      (setq langtool-java-classpath
      "/usr/share/languagetool:/usr/share/java/languagetool/*")
      (require 'langtool)
      (setq langtool-default-language "en-US")
      )
    )
