;; pkg: which key
(use-package which-key
  :ensure t
  :config
  (which-key-mode)
  ;; hide mode line
  (setq-default which-key-lighter ""))

(provide 'init-mini)
