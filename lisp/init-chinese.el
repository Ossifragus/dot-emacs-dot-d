;;; Chinese input
(use-package pyim
  :ensure t
  :config
  (setq default-input-method "pyim")
  (setq pyim-page-length 10)
;;; fuzzy pinyin
  (setq pyim-fuzzy-pinyin-alist
        '(("c" "ch")
          ("s" "sh")
          ("z" "zh")
          ("en" "eng")
          ("in" "ing"))
        )
  )
(use-package pyim-basedict
  :ensure t
  :config
  (pyim-basedict-enable))

(provide 'init-chinese)
;;; init-chinese.el ends here
