;; julia-mode
(use-package julia-mode
  :ensure t
  :config
  (add-to-list 'auto-mode-alist '("\\.jl\\'" . julia-mode))
)

(use-package julia-repl
  :ensure t
  :config
  (require 'julia-repl)
  (add-hook 'julia-mode-hook 'julia-repl-mode) ;; always use minor mode
  (set-language-environment "UTF-8")
  (julia-repl-set-terminal-backend 'vterm)
  )

(use-package julia-snail
  :ensure t
  :requires vterm
  :hook (julia-mode . julia-snail-mode)
  )


(provide 'init-julia)
;;; init-julia.el ends here
