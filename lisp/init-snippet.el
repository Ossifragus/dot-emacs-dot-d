;;; use yasnippet-snippets
(use-package yasnippet
  :ensure t
  :config (yas-global-mode 1))
(use-package yasnippet-snippets
  :ensure t)

(provide 'init-snippet)
;;; init-snippet.el ends here
