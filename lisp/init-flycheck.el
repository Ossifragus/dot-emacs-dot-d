(use-package flycheck
  :ensure t
  :config
  (add-hook 'after-init-hook 'global-flycheck-mode)
  (setq flycheck-display-errors-function
        #'flycheck-display-error-messages-unless-error-list)
  ;;; flycheck with pylint
  (setq flycheck-python-pylint-executable "pylint")
  )

(provide 'init-flycheck)
