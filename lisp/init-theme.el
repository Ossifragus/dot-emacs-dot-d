;; load theme - pkg: color-theme-sanityinc-tomorrow
(use-package color-theme-sanityinc-tomorrow
  :ensure t
  :init
  (setq custom-safe-themes t)
  :config
  (require 'color-theme-sanityinc-tomorrow)
  (setq-default custom-enabled-themes '(sanityinc-tomorrow-eighties))
  (color-theme-sanityinc-tomorrow--define-theme eighties)
  (load-theme 'sanityinc-tomorrow-eighties t))

(provide 'init-theme)
