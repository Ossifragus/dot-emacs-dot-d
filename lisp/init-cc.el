;;; using another indentation style
(c-add-style "ellemtel-offset4"
             '("ellemtel"
               (indent-tabs-mode . nil)        ; use spaces rather than tabs
               (c-basic-offset . 4)            ; indent by four spaces
               (c-indent-level . 4)
               ))

(defun my-c++-mode-hook ()
  "Use my-style defined above."
  (c-set-style "ellemtel-offset4")
  )
(add-hook 'c++-mode-hook 'my-c++-mode-hook)

;;; use c++-mode by default for .h files
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))

;;; require irony
(use-package irony
  :ensure t)
(add-hook 'c++-mode-hook 'irony-mode)
(add-hook 'c-mode-hook 'irony-mode)
(add-hook 'objc-mode-hook 'irony-mode)
(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

(defun my-irony-mode-hook ()
  "Update the `completion-at-point' and `complete-symbol' bindings."
  (define-key irony-mode-map [remap completion-at-point]
    'irony-completion-at-point-async)
  (define-key irony-mode-map [remap complete-symbol]
    'irony-completion-at-point-async))
(add-hook 'irony-mode-hook 'my-irony-mode-hook)

;;; use irony eldoc
(use-package irony-eldoc
  :ensure t)
(add-hook 'irony-mode-hook #'irony-eldoc)

(defun my-company-irony ()
  "Avoid enabling irony-mode in modes that inherits 'c-mode', e.g: 'php-mode'."
  (when (eq major-mode 'c-mode)
    (irony-mode)
    (unless (memq 'company-irony company-backends)
      (setq-local company-backends (cons 'company-irony company-backends)))))

;;; using company-irony and company-irony-c-headers
(use-package company-irony
  :ensure t)
(use-package company-irony-c-headers
  :ensure t)
(add-hook 'irony-mode-hook 'company-irony-setup-begin-commands)
(setq company-backends (delete 'company-semantic company-backends))
(eval-after-load 'company
  '(add-to-list 'company-backends '(company-irony company-irony-c-headers)))
;; (setq company-idle-delay 0.5)

;; set R-lib
(setq r-lib-suffix "/usr/local/lib/R/site-library/")
(setq r-lib-path "/usr/include/R/")
(when *is-a-mac*
  (setq r-lib-suffix "/Library/Frameworks/R.framework/Versions/4.0/Resources/library/")
  (setq r-lib-path "/Library/Frameworks/R.framework/Resources/include/"))

;;; flycheck
(add-hook 'c++-mode-hook
          (lambda ()
            (setq flycheck-gcc-language-standard "c++11")
            ;; (setq flycheck-disabled-checkers '(c/c++-clang))
            ;; (setq flycheck-select-checker "c/c++-gcc")
            (setq flycheck-select-checker "c/c++-clang")
            ;; add R, Rcpp, and its friends
            (setq flycheck-clang-include-path
                  (list
                   r-lib-path
                   (concat r-lib-suffix "Rcpp/include")
                   (concat r-lib-suffix "RcppArmadillo/include")
                   (concat r-lib-suffix "RcppEigen/include")
                   )
                  )
            )
          )

;;; flycheck-irony seems to miss warnings such as unused variables
;;; and need to set up .clang_complete for Rcpp
;;; so it is disabled now in favor of native flycheck
;; (require-package 'flycheck-irony)
;; (eval-after-load 'flycheck
;;   '(add-hook 'flycheck-mode-hook #'flycheck-irony-setup))

(provide 'init-cc)
