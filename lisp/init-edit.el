(global-set-key (kbd "RET") 'newline-and-indent)

(when (fboundp 'electric-pair-mode)
  (add-hook 'after-init-hook 'electric-pair-mode))

(setq-default grep-highlight-matches t
              grep-scroll-output t)

(when *is-a-mac*
  (setq-default locate-command "mdfind"))

;; For auto-fill-mode
(setq-default fill-column 80)

;; highlight pairs
(add-hook 'after-init-hook 'show-paren-mode)

(setq-default
 blink-cursor-interval 0.4
 bookmark-default-file (expand-file-name ".bookmarks.el" user-emacs-directory)
 buffers-menu-max-size 30
 case-fold-search t
 column-number-mode t
 delete-selection-mode t
 ediff-split-window-function 'split-window-horizontally
 ediff-window-setup-function 'ediff-setup-windows-plain
 indent-tabs-mode nil
 create-lockfiles nil
 auto-save-default nil
 make-backup-files nil
 mouse-yank-at-point t
 save-interprogram-paste-before-kill t
 scroll-preserve-screen-position 'always
 set-mark-command-repeat-pop t
 tooltip-delay 1.5
 truncate-lines nil
 truncate-partial-width-windows nil)

(add-hook 'after-init-hook 'global-auto-revert-mode)
(setq global-auto-revert-non-file-buffers t
      auto-revert-verbose nil)

;; Rectangle selections, and overwrite text when the selection is active
(cua-selection-mode t)
;; disable cua rectangle mark
(define-key cua-global-keymap [C-return] nil)

(defun fill-sentence ()
  "Fill only the current sentence."
  (interactive)
  (save-excursion
    (backward-sentence)
    (push-mark)
    (forward-sentence)
    (if (eq major-mode 'org-mode)
        (org-fill-region (point) (mark))
      (fill-region (point) (mark))
      )
    )
  )
(global-set-key (kbd "M-q") 'fill-sentence)

;; pkg: unfill
(use-package unfill
  :ensure t)

;; pkg: vlf
(use-package vlf
  :ensure t)
(defun ffap-vlf ()
  "Find file at point with VLF."
  (interactive)
  (let ((file (ffap-file-at-point)))
    (unless (file-exists-p file)
      (error "File does not exist: %s" file))
    (vlf file)))

;; multiple-cursors
(use-package multiple-cursors
  :ensure t)
(define-key mc/keymap (kbd "<return>") nil) ; use C-g to quit only
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-+") 'mc/mark-next-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
;; From active region to multiple cursors:
(global-set-key (kbd "C-c m r") 'set-rectangular-region-anchor)
(global-set-key (kbd "C-c m c") 'mc/edit-lines)
(global-set-key (kbd "C-c m e") 'mc/edit-ends-of-lines)
(global-set-key (kbd "C-c m a") 'mc/edit-beginnings-of-lines)

(use-package rainbow-delimiters
  :ensure t
  :config
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

(use-package symbol-overlay
  :ensure t
  :delight
  :config
  (dolist (hook '(prog-mode-hook html-mode-hook yaml-mode-hook conf-mode-hook))
    (add-hook hook 'symbol-overlay-mode))
  (define-key symbol-overlay-mode-map (kbd "M-i") 'symbol-overlay-put)
  (define-key symbol-overlay-mode-map (kbd "M-I") 'symbol-overlay-remove-all)
  (define-key symbol-overlay-mode-map (kbd "M-n") 'symbol-overlay-jump-next)
  (define-key symbol-overlay-mode-map (kbd "M-p") 'symbol-overlay-jump-prev))

(use-package column-enforce-mode
  :ensure t
  :config
  (add-hook 'ess-mode-hook (lambda () (interactive) (column-enforce-mode)))
  (add-hook 'c++-mode-hook (lambda () (interactive) (column-enforce-mode))))

(use-package disable-mouse
  :ensure t)

(use-package editorconfig
  :ensure t
  :config
  (editorconfig-mode 1)
  ;; hide mode line
  (setq-default editorconfig-mode-lighter ""))

(provide 'init-edit)
