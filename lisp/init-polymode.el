(use-package markdown-mode
  :ensure t
  :mode ("\\.md\\'"))

;; require polymode
(use-package polymode
  :ensure t)
(use-package poly-markdown
  :ensure t
  :config
  (require 'poly-markdown))
(use-package poly-R
  :ensure t
  :config (require 'poly-R))
(use-package poly-noweb
  :ensure t
  :config (require 'poly-noweb))

;;; insert code chunk
;;; https://emacs.stackexchange.com/questions/27405/insert-code-chunk-in-r-markdown-with-yasnippet-and-polymode
(defun rmd-insert-r-chunk (header)
  "Insert an r-chunk HEADER in markdown mode.
Necessary due to interactions between polymode and yas snippet."
  (interactive "sHeader: ")
  (insert (concat "```{r " header "}\n\n```"))
  (forward-line -1))

(custom-set-variables
 '(markdown-command "/usr/bin/pandoc"))

(provide 'init-polymode)
