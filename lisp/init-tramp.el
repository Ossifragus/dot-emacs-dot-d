;; for tramp
(require 'tramp)
(setq tramp-default-method "ssh")
(add-to-list 'tramp-remote-path 'tramp-own-remote-path)
;;; speed up completions
(setq tramp-completion-reread-directory-timeout nil)
;;; disable version control to avoid delays:
;; (setq vc-ignore-dir-regexp
;;       (format "\\(%s\\)\\|\\(%s\\)"
;;               vc-ignore-dir-regexp
;;               tramp-file-name-regexp))
(setq vc-handled-backends '(Git))

(provide 'init-tramp)
;;; init-tramp.el ends here
